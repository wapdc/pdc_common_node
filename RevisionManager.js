import fs from 'fs'
import path from 'path'
import util from 'util'
import childProcess from 'child_process'
const exec = util.promisify(childProcess.exec)
const chmod = util.promisify(fs.chmod)
const stat = util.promisify(fs.stat)
const readdir = util.promisify(fs.readdir)
import * as db from "./WapdcDb.js"

let script_directory, app_name, currentVersion = ''
async function setup(dir, appName){

script_directory = dir
app_name = appName
}

async function setExecutable(path){
  const stats = await stat(path)
  if (stats.isFile() && !(stats.mode & 1)) {
    return chmod(path, '755')
  }
}

async function checkAndSetPermissions(dir) {
  const files = await readdir(dir)
  const filePaths = files.map(file => path.join(dir, file))
  await Promise.all(filePaths.map(setExecutable))
}

  async function getVersion() {
    try {
      const res = await db.fetch("SELECT version FROM schema_revision WHERE application = $1", [app_name])
      if (res){
        return res.version
      } else {
        return ""
      }
    } catch (error) {
      throw new Error(`Error in getVersion: ${error.message}`)
    }
  }

  async function setVersion(version) {
    const current_version = await getVersion()
    try {
      if (current_version) {
        await db.execute("UPDATE schema_revision SET version = $1 WHERE application = $2", [version, app_name])
      } else {
        await db.execute("INSERT INTO schema_revision(application, version) VALUES ($1, $2)", [app_name, version])
      }
    } catch (error) {
      throw new Error(`Error in setVersion: ${error.message}`)
    }
  }

  async function runScript(file) {
    try {
      const res = await exec(`sh ${file}`, {
        cwd: ".."
      })
      console.log(res.stdout)
      return true
    } catch (error) {
      throw new Error(`Error in runScript: ${error.message}`)
    }
  }

export async function runRevScripts(dir, appName) {

  //setup
  await setup(dir, appName)

  currentVersion = await getVersion()
  console.log(`Executing revision scripts from ${script_directory}`)
  console.log(`Current Version: ${currentVersion}\n`)

  await checkAndSetPermissions(script_directory)

  const files = await readdir(script_directory) // changed fs.readdirSync to readdir
  const filteredFiles = files.filter(file => /^rev\..*$/g.test(file))

  for (const file of filteredFiles) {
    // noinspection JSUnusedLocalSymbols
    const [prefix, version, comment, ext] = file.split('.')
    if (!currentVersion || currentVersion < version) {
      console.log(`Executing ${file} ...\n`)
      await runScript(path.join(script_directory, file))
      await setVersion(version)
      const updatedVersion = await getVersion() // Fetch the updated version
      console.log(`Current Version Updated: ${updatedVersion}\n`)
    }
  }
}

