# pdc_common_node

This library is meant for use in AWS lambdas that are written by the Washington Public Disclosure commission. If you are 
setting up a project for use with this library for the first time see the [AWS Project Setup](doc/AWS-project-setup.md) documentation. 

## Table of Contents

- [Project setup](#project-setup)
    - [Node version management](#node-version-management)
    - [Updating the version of node](#updating-the-version-of-node)
- [Run unit tests](#run-unit-tests)
- [secrets-manager](#secrets-manager)
- [wapdc-db](#wapdc-db)
    - [Connections](#connections)
    - [Querying data](#querying-data)
    - [Transactions](#transactions)
- [mailer](#mailer)
    - [send Message](#send-message)
- [revision-manager](#revision-manager)
    - [runRevScripts](#runrevscripts)
- [signer](#signer)
    - [sign](#sign)
    - [decode](#decode)
    - [verify](#verify)
- [rest-api](#rest-api)
    - [claims (object)](#claims-object)
    - [getRestApi](#getrestapi)
    - [waitForCompletion](#waitforcompletion)
    - [setCORSHeaders](#setcorsheaders)
    - [getClaims](#getclaims)
- [aws-gateway](#aws-gateway)
    - [authorizer](#authorizer)
        - [authorize vs authorizeAdmin](#authorize-vs-authorizeadmin)
    - [registerUrl](#registerurl)
- [drafts](#drafts)
    - [createDraft](#createdraft)
    - [saveDraft](#savedraft)
    - [getDraft](#getdraft)
    - [deleteDraft](#deletedraft)   

## Project setup

### Node version management
Use NVM for managing your Node versions. The PHPStorm Jest test configuration template expects to find the node binary 
on a path managed by NVM. It expects a specific/exact path.

If you run tests via PHPStorm and get a message that the not interpreter cannot be found, you probably don't have the 
version of node that is prescribed in the Jest test run template. If that's the case, use `nvm install` and/or `nvm use` 
to install the correct exact version. PHPStorm only requires the `nvm install`. Using `nvm use` will also make sure that 
you are using the same version when using the command line.

### Updating the version of node
If you think the version of node should be updated for the project and tests, update the node version using nvm install
and update the PHPStorm run configuration template to specify that version. You either need to remove any specific run 
configurations and recreate them from the updated template or update all your configurations. Be sure to push the 
changes to the template which are in the .run directory at the root of the project.

## Run unit tests
Use command `npm test`.

## secrets-manager
These are functions used to interact with the AWS secrets manager.

```js
import {getSecrets} from '@wapdc/common/secrets-manager'

//It retrieves the secret value for the AWS secret name
const secrets = getSecrets('MySecretName')
```

## wapdc-db
Module to provide access to the wapdc database.  Because we so often use multiple functions
in the databse library, the most common method of importing the library would be as follows: 

```js
import * as db from '@wapdc/common/wapdc-db'
```

### Connections 

Establishes a connection to the database.  Note that it is important when managing connections that you need to always 
call db.close(), even with error conditions. One way to accomplish this would be to implement all database connectivity 
it a try/catch/finally as indicated below. 

```js
try {
  await db.connect();
 
 
  // Make multiple database calls here. 
}
catch (e) {
  // Optional error handling code goes here. 
}
finally {
  await db.close();
}
```

### Querying data 

```js
// Fetch a single row from the database
//   returns single row as an object
const row = await db.fetch(`select * from election where election_code=$1`, ['2024'])

// Fetch multilple rows from the databse and return as an array of objects
const rows = await db.query(`select * from election where election_year=$1`, [2024]); 

// Excecute a statement without returing a result
await db.execute(`insert into wapdc_settings(property, stage, value) values ('myprop', 'dev','x')`)

// Get a peroperty from the wapdc_settings using the database stage
const gateway_url = await db.getWapdcSetting('lmc_aws_gateway')

// Set a property in the wapdc settings table.  
// This is often used to register gateway URL's in lambda functions
await db.setWapdcSetting('lmc_aws_gateway', 'http://localhost:3000')
```

### Transactions
In the rare cases where you need transactions (commit and rollback), the following pattern can be used.

```js
try {
  db.beginTransaction()
  // perform multiple database calls. 
  db.commit(); 
}
catch {
  db.rollback(); 
}
```

## mailer
There are a set of functions to allow sending of mail via sendgrid api.

### send Message

sends the same email content to one or more email addresses.

currently only sends emails to `@pdc.wa.gov` recipients when not in live.

- contact - can be a single string containing a single email address or an array.
- templateData - is the data that will be consumed by the template.

```js
import {sendMessage} from '@wapdc/common/mailer'

const contact = 'example@email.com'
const templateId = 'a-bbbbbbbbbbbbbbbbbbbbbs' 
const templateData = {
    committee: 'friends of john jones', 
    candidate: 'john jones'
}
await sendMessage(contact, templateId, templateData)
```

## revision-manager
Functionality for installing the latest revision scripts for postgres database

### runRevScripts

Updates the schema_revision when the current version is less than any file in the target directory. This is used along
with wapdc-db to utilize connection to the database.

- scriptDirectory - location of where the revision scripts are stored (project/revisions)
- projectName - name of the project that will be inserted into the schema_revision table in the rds

```js
import {runRevScripts} from "../../RevisionManager.js"
import * as db from "../../WapdcDb.js"

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));
const scriptDirectory = path.join(__dirname, '..', 'data/revisionTest')
const projectName = 'myProject'

await db.connect()
await db.beginTransaction()
await runRevScripts(scriptDirectory, projectName)
await db.commit()
await db.close()

```

## signer
Functionality of signing tokens (JWT) using the imported jose library using a secret hmac_key. The functionalities included
are decoding, signing, and verifying.

```js
/**
 * one way to blanket import signer
 */
import * as signer from "@wapdc/common/signer"
```

### sign
This method grabs the hmac_key secret and signs the payload outputting an encrypted token.

- payload - The data you are signing, i.e. campaign finance backup files
- keyName - The secret key you are signing with, i.e. TokenSigner
- expiration - Optional, per jose documentation it can be passed as a string or a timestamp
- token - the encrypted file created

```js
import {sign} from "@wapdc/common/signer"

const somePayload = {
    "committee_id": 100,
    "fund_id": 110,
    "etc": 'miscellaneous'
}
//sets expiration to 2 hours from now
const token = sign(somePayload, keyName, "2h")
```

### decode
Decodes a signed JSON Web Token payload. This does not validate the JWT Claims Set types or values. 
This does not validate the JWS Signature. For a proper Signed JWT Claims Set validation and JWS signature 
verification use verify(). Used as a quick checker, not for authorization purposes.

- token - the JWT generated from signing the payload

```js
import {decode} from "@wapdc/common/signer"

/**
 * payload will be the same as the somePayload above
 */
const payload = decode(token)
```

### verify
Verifies the JWT format (to be a JWS Compact format), verifies the JWS signature, validates the JWT Claims Set. Returns
the payload. Used for authorization of claims and structure.

- token - the JWT generated from signing the payload
- keyName - The secret key you are signing with, i.e. TokenSigner

```js
import {verify} from "@wapdc/common/signer"

/**
 * payload will be the same as the somePayload above
 */
const payload = verify(token, keyName)
```

## rest-api
Establishes the lambda-api, establishes error handling via getting claims and setting CORS headers. 

```js
import {claims, setCORSHeaders, getClaims, getRestApi} from "@wapdc/common/rest-api"
```

### claims (object)
User claims from either local file or from event authorizer.

- uid - unique identifier for user
- realm - location of where the user was created/logged
- admin - flag indicating admin privileges

```js
/**
 * Base structure of claims object
 */

const claims = {
        uid: null,
        realm: null,
        admin: false
    }
```

### getRestApi


```js
import {claims, getRestApi} from "@wapdc/common/rest-api"

/**
 * Inside getRestApi sets the CORSHeaders, gets the claims and sets it to the claims variable
 * claims will reflect the requested user data
 */
const api = getRestApi({compression: true})
```

Note that if you use the compression true option above the following items need to be added to the template.yaml file that 
defines the aws gateway in order for local dev environments to function. 

After the property definitions define a condition for determining dev environment. 
```yaml
Conditions:
  IsDevEnvironment: !Equals [!Ref Stage, "dev"]
```

In the API Gateway definition add the following property: 
```yaml
      # Enable binary returns for all content types only in the dev environment.       
      BinaryMediaTypes:
        - !If [IsDevEnvironment, "*/*", "application/octet-stream"]
```

### waitForCompletion
Waits for the completion of a prior call to api.run. Because database closes are async calls and the API's 
finally method provided by the lambda-api doesn't support async, we need to wait for the database to close at every request. 
This function must be awaited prior to returning in any lambda handler

```javascript
  import {waitForCompletion} from "@wapdc/common/rest-api"
  await waitForCompletion()
```

### setCORSHeaders
Function to set CORS headers.

- res - the HTTP response object

### getClaims
Fetches user claims from either local file or from event authorizer.

- req - The request object from the lambda-api

## aws-gateway
Set of functions used to generate an AWS IAM policy based on the user, with authorization layer prior to creation.\
AWS Documentation: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-handler.html

```js
import {authorize, authorizeAdmin, registerUrl} from '@wapdc/common/aws-gateway'
```

### authorizer
Checks to see if the authorize.json contains the appropriate token. Once extracted, checks if the payload contains the 
appropriate claims and generates the policy. The parameters are defined from the aws api

- event - The invoker(information) passes as JSON when it calls Invoke, and the runtime converts it to an object.
- context - Contains information about the invocation, function, and execution environment.
- callback - Takes two arguments: an Error and a response. The response object must be compatible with JSON.stringify.

#### authorize vs authorizeAdmin
- authorize is the filer level permissions (default)
- authorizeAdmin expects admin level privileges

```js
import * as authorizer from '@wapdc/common/aws-gateway'

/**
 * Can define it like this without having to explicitly pass the required parameters expected by aws (event, context, callback)
 */
const authorize = authorizer.authorize

const authorizeAdmin = authorizer.authorizeAdmin
```

### registerUrl
This is a helper function that writes the url to the wapdc_settings table when the stack is created, due to the fact that
the url can change this function runs post build so that the database url stays up-to-date. 

- payload - The output is expected to be the json that is generated from the aws cloud formation describe-stacks command for the deployed instance.
- gatewayProperty - The name of the property wanted to be inserted into the wapdc_settings table (i.e. 'c5_aws_gateway')

```js
import {registerUrl} from "@wapdc/common/aws-gateway";

const gatewayProperty = 'c5_aws_gateway'

export async function registerUrl(event) {
    return registerUrl(event, gatewayProperty)
}
```

## Drafts

This section provides functions for creating, saving, getting, and deleting drafts in the database.

### createDraft

Creates a new draft in the database and returns the draft_id.

```js
import { createDraft } from '@wapdc/common/drafts';

// example draft object
const draft = {
  target_type: 'example_type',
  target_id: 123,
  user_data: { key: 'value' },
  uid: claims.uid,
  realm: claims.realm,
  report_type: 'example_report',
  report_key: 'example_key'
};

const draftId = await createDraft(draft);
console.log(draftId);
```

### saveDraft

Updates an existing draft and increments its save count. Returns the new save count.

```js
import { saveDraft } from '@wapdc/common/drafts';

const draft = {
  draft_id: 1,
  user_data: { updated_key: 'updated_value' },
  save_count: 0
};

const newSaveCount = await saveDraft(draft);
console.log(newSaveCount);
```

### getDraft

Retrieves a draft from the database by its ID. Returns the draft object or null if not found.

```js
import { getDraft } from '@wapdc/common/drafts';

const draftId = 1;
const draft = await getDraft(draftId);
console.log(draft);
```

### deleteDraft

Deletes a draft from the database by its ID.

```js
import { deleteDraft } from '@wapdc/common/drafts';

const draftId = 1;
await deleteDraft(draftId);
```
