import * as signer from './Signer.js'
import * as db from "./WapdcDb.js"

/**
 * Help function to generate IAM policy
 */
function generatePolicy(principalId, effect, methodArn, context) {
    let authResponse = {}

    /**
     * The following resource wildcard generation code came from: https://repost.aws/knowledge-center/api-gateway-lambda-authorization-errors
     * And is used to make sure that we generate an appropriate wildcard resource when generating authorization requests that
     * work with the proxy+ gateway mapping.
     */
    let tmp = methodArn.split(':')
    let apiGatewayArnTmp = tmp[5].split('/')
    let resource = tmp[0] + ":" + tmp[1] + ":" + tmp[2] + ":" + tmp[3] + ":" + tmp[4] + ":" + apiGatewayArnTmp[0] + '/*/*'

    authResponse.principalId = principalId
    if(effect && resource) {
        let policyDocument = {}
        policyDocument.Version = '2012-10-17'
        policyDocument.Statement = []
        let statementOne = {}
        statementOne.Action = 'execute-api:Invoke'
        statementOne.Effect = effect
        statementOne.Resource = resource
        policyDocument.Statement[0] = statementOne
        authResponse.policyDocument = policyDocument
    }

    //Optional output with custom properties of the String, Number or Boolean type
    authResponse.context = context
    return authResponse
}

/**
 * Checks to see if the authorize.json contains the appropriate token
 * Once extracted, checks if the payload contains the appropriate claims and generates the policy
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<void>}
 */

export const authorize = async(event, context, callback) => {
    const token = event.authorizationToken.replace('Bearer ', '')
    let effect, payload
    try {
        payload = await signer.verify(token, 'TokenSigner')
        effect = 'Allow'
    } catch(e) {
        console.error(e)
        payload = null
    }
    if (!payload || !payload.uid) {
        console.error("Access denied: ", signer.decode(token))
        effect = 'Deny'
        payload = {}
    }

    const policy = generatePolicy('user', effect, event.methodArn, payload)
    callback(null,policy)
}

/**
 * Checks to see if the authorize.json contains the appropriate token
 * Once extracted, checks if the payload contains the appropriate claims and generates the policy
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<void>}
 */

export const authorizeAdmin = async(event, context, callback) => {
    const token = event.authorizationToken.replace('Bearer ', '')
    let effect, payload
    try {
        payload = await signer.verify(token, 'TokenSigner')
        effect = 'Allow'
    } catch(e) {
        console.error(e)
        payload = null
    }
    if (!payload || !payload.admin ) {
        console.error("Access denied: ", signer.decode(token))
        effect = 'Deny'
        payload = {}
    }

    const policy = generatePolicy('user', effect, event.methodArn, payload)
    callback(null,policy)
}

/**
 * @param event
 *  The payload for this output is expected to be the json that is
 *  generated from the aws cloud formation describe-stacks command for the
 *  deployed instance.
 *
 * @param gatewayProperty
 *  The name of the property wanted to be inserted into the wapdc_settings table
 *  i.e. c5_aws_gateway
 * @returns {Promise<void>}
 */
export const registerUrl = async(event, gatewayProperty) => {
    console.info("Registration event", event)
    if (!event && !event.Stacks) {
        throw Error("Invalid or missing event")
    }
    const stack = event.Stacks[0]
    if (!stack || !stack.Outputs) {
        throw Error("Cannot find stack output to extract url")
    }
    const output = stack.Outputs.find(o => o.OutputKey === "ApiGatewayUrl")
    if(!gatewayProperty) {
        throw Error("Invalid or missing gatewayProperty")
    }
    await db.connect()
    await db.setWapdcSetting(gatewayProperty, output.OutputValue)
}