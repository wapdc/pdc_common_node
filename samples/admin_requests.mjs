import {claims, getRestApi, waitForCompletion} from "pdc-common/rest-api"

/**
 * Call the getRestAPI to initialize the api.
 */
const api = getRestApi()

/** Ensure that all admin pages require access */
api.use('admin/*', (req, res, next) => {
  if (!claims.admin) {
    return res.error({ type: "Access denied", message: "Access denied (admin)", statusCode: 403 })
  }
  next()
})

api.get('/hello', async (req, res) => {
  await res.json({message: "Hello world", claims: claims});
})

/**
 * AWS Lambda handler. Runs the API with the provided event and context.
 *
 * @param {object} event - The AWS Lambda event object.
 * @param {object} context - The AWS Lambda context object.
 * @returns {Promise} - The promise to return a response.
 */
export const lambdaHandler = async (event, context) => {
  const result =  await api.run(event, context)
  await waitForCompletion()
  return result
}
