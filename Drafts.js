import { execute, fetch } from './WapdcDb.js';

/**
 * Creates a new draft in the database.
 * @param {Object} draft - The draft object to be inserted.
 * @returns {Promise<number>} The ID of the created draft.
 * @throws {Error} If there is an error creating the draft.
 */
export async function createDraft(draft) {
  try {
    const insertQuery = `
      INSERT INTO private.draft_document (
        target_type,
        target_id,
        save_count,
        user_data,
        uid,
        realm,
        updated_at,
        report_type,
        report_key
      ) 
      VALUES ($1, $2, 0, $3, $4, $5, NOW(), $6, $7)
      RETURNING draft_id
    `;

    const values = [
      draft.target_type,
      parseInt(draft.target_id, 10),
      JSON.stringify(draft.user_data),
      draft.uid,
      draft.realm,
      draft.report_type,
      draft.report_key
    ];

    const { draft_id } = await fetch(insertQuery, values);
    return draft_id;
  } catch (error) {
    console.error('Error creating draft:', error.message);
    throw new Error('Failed to create draft');
  }
}

/**
 * Updates an existing draft and increments its save count.
 * @param {Object} draft - The draft object to be updated.
 * @returns {Promise<number>} The new save count of the draft.
 * @throws {Error} If the draft is not found, if there is a save count mismatch.
 */
export async function saveDraft(draft) {
  let currentDraft;
  if (draft.draft_id) {
    currentDraft = await getDraft(draft.draft_id)
  }
  else {
    currentDraft = await getReportDraft(draft.target_type, draft.target_id, draft.report_type, draft.report_key)
  }

  if (!currentDraft) {
    throw new Error('Draft not found');
  }

  if (currentDraft.save_count !== draft.save_count) {
    throw new Error('Save count mismatch');
  }

  const newSaveCount = draft.save_count + 1;
  await execute(`
    UPDATE private.draft_document 
    SET user_data = $1, save_count = $2, updated_at = NOW()
    WHERE draft_id = $3`,
    [JSON.stringify(draft.user_data), newSaveCount, parseInt(currentDraft.draft_id, 10)]
  );

  return newSaveCount;
}

/**
 * Retrieves a draft from the database by its ID.
 * @param {number} draft_id - The ID of the draft to retrieve.
 * @returns {Promise<Object|null>} The retrieved draft object or null if not found.
 * @throws {Error} If there is an error retrieving the draft.
 */
export async function getDraft(draft_id) {
  const draft = await fetch(`
    SELECT * FROM private.draft_document 
    WHERE draft_id = $1`,
    [parseInt(draft_id, 10)]);
  return draft ? draft : null;
}

/**
 * Retrieves a draft form the database by unique composite keys.  Used for draft reports that have a report_key.
 * @param {String} target_type target type of firm being loaded
 * @param {int} target_id
 * @param {String} report_type type of repor
 * @param {String }report_key Uniquey key for report within target_type, target_id, report_type
 * @return {Promise<*|null>}
 */
export async function getReportDraft(target_type, target_id, report_type, report_key){
  const draft = await fetch(`
    SELECT * FROM private.draft_document 
    WHERE target_type = $1
      and target_id = $2
      and report_type = $3
      and report_key = $4`,
    [target_type, parseInt(target_id, 10),report_type, report_key]);
  return draft ? draft : null;
}



/**
 * Deletes a draft from the database by its ID.
 * @param {number} draft_id - The ID of the draft to delete.
 * @returns {Promise<void>}
 * @throws {Error} If there is an error deleting the draft.
 */
export async function deleteDraft(draft_id) {
  await execute(
    `DELETE FROM private.draft_document WHERE draft_id = $1`,
    [parseInt(draft_id, 10)]
  );
}

export async function deleteReportDraft(target_type, target_id, report_type, report_key) {
  await execute(
    `DELETE FROM private.draft_document 
       WHERE target_type=$1 AND target_id=$2 and report_type=$3 and report_key=$4 `,
    [target_type, parseInt(target_id, 10), report_type, report_key]
  );
}