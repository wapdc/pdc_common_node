import {getSecrets} from "./SecretsManager.js"
import sgMail from "@sendgrid/mail"
import client from "@sendgrid/client"


const messageBase = {
    from: 'pdc@pdc.wa.gov'
}

let initialized = false
let apiKey = null

async function initialize() {
    if (!initialized) {
        try {
            if (process.env.STAGE !== 'dev') {
                apiKey = (await getSecrets('SendGrid')).api_key
            } else {
                apiKey = process.env.SENDGRID_SECRET
            }
            //Checks for apiKey before attempting to set it
            if (apiKey) {
                sgMail.setApiKey(apiKey)
            }
            initialized = true
            //If any issues happened in getSecrets or setApiKey will be caught here
        } catch (e) {
            console.info("Exception: ", e)
        }
    }
}

export async function sendMessage(contact, templateId, templateData) {
    await initialize()
    if (process.env.STAGE !== 'live' && process.env.STAGE !== 'prod' && testEmailAddress) {
        contact = testEmailAddress
    }
    // This filters out all non-pdc contacts in the demo/test environments
    // If there are no recipients, then it just silently returns
    if (process.env.STAGE !== 'live' && process.env.STAGE !== 'prod') {
        if (Array.isArray(contact)) {
            contact = contact.filter((x) => x.toLowerCase().includes('@pdc.wa.gov'))
        } else {
            contact = contact.toLowerCase().includes('@pdc.wa.gov') ? contact : null
        }
    }

    const msg = Object.assign(messageBase)
    msg.to = contact
    msg.templateId = templateId
    msg.dynamicTemplateData = templateData
    const hasRecipients = Array.isArray(contact) ? contact.length > 0 : !!contact

    if (hasRecipients && apiKey) {
        try {
            await sgMail.send(msg);
        } catch (error) {
            console.error(error);

            if (error.response) {
                console.error(error.response.body)
            }
            throw (error)
        }
    } else {
        console.info("Skipping mail send. apiKey present: " + !!apiKey, " Recipients value: " + hasRecipients)
    }
}

let mailMergeMessage
export async function setMessageTemplate(templateId, bypassUnsubscribe = false) {
    await initialize()
    mailMergeMessage = {
        from: 'pdc@pdc.wa.gov',
        templateId: templateId,
        personalizations: [],
        mail_settings: {
            bypass_spam_management: {
                enable: bypassUnsubscribe
            },
            bypass_unsubscribe_management: {
                enable: bypassUnsubscribe
            }
        }
    }
}

let testEmailAddress;
export function setTestEmailAddress(email) {
    testEmailAddress = email;
}

export function addRecipient(email, data) {
    if (process.env.STAGE !== 'live' && process.env.STAGE !== 'prod' && testEmailAddress) {
        email = testEmailAddress
    }
    mailMergeMessage.personalizations.push({
        to: email,
        dynamicTemplateData: data
    })
}


export async function sendMailMerge() {
    try {
        await sgMail.send(mailMergeMessage);
    } catch (error) {
        console.error(error);

        if (error.response) {
            console.error(error.response.body)
        }
        throw (error)
    }
}

export async function getTemplates() {
    await initialize();
    client.setApiKey(apiKey)
    const request = {
        method: 'GET',
        url: '/v3/templates',
        qs: {
            'page_size': '200',
            'generations': 'dynamic'
        }
    }
    const [response] = await client.request(request)
    if (response.statusCode===200){
        return response.body.result
    }
    else {
        return []
    }
}
