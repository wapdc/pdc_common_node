import {test, describe, afterAll, beforeAll} from "@jest/globals"
import {runRevScripts} from "../../RevisionManager.js"
import url from "url"
import path from "path"
import * as db from "../../WapdcDb.js"

beforeAll(async () => {
    await db.connect()
    await db.beginTransaction()
})
afterAll(async () => {
    await db.rollback()
    await db.close()
})
describe('RevisionsManager', () => {

    const __dirname = url.fileURLToPath(new URL('.', import.meta.url))
    const scriptDirectory = path.join(__dirname, '..', 'data/revisionTest')

    test('Run rev script', async () => {

        await runRevScripts(scriptDirectory, 'jest-test')
        const jest_schema = await db.fetch(`select version from wapdc.public.schema_revision where application = 'jest-test'`)
        expect(jest_schema.version).toBe('2023-11-001')

    })
})