import * as db from '../../WapdcDb.js'
import {afterAll, beforeAll, describe} from "@jest/globals"

beforeAll(async () => {
  await db.connect()
  await db.beginTransaction()
})

afterAll(async () => {
  await db.rollback()
  await db.close()
})
describe('Can perform basic database queries',  () => {

  test('Can execute database query', async () => {
    const rows = await db.query(`SELECT x from generate_series(1, 10) x where x <= $1`, [5])
    expect(rows).toBeTruthy()
    expect(rows.length).toBe(5)
  })

  test( 'Can fetch single row', async () => {
    const row = await db.fetch(`SELECT 'blue' as color where $1='blue'`, ['blue'])
    expect(row).toStrictEqual({color: "blue"})
  })

  test('can get and set settings', async () => {
    await db.setWapdcSetting('test_value', 'foo')
    const value = await db.getWapdcSetting('test_value')
    expect(value).toBe('foo')
  })

  test('Can execute a statement', async () => {
    await db.execute(`create temporary table test_foo(a text)`)
    await db.execute('insert into test_foo(a) values ($1)', ['a'])
    const test_foo = await db.fetch(`select a from test_foo`)
    expect(test_foo.a).toBe('a')
  })
})