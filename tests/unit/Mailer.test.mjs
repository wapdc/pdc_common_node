import {test, describe, expect, jest} from "@jest/globals"
import {sendMessage} from '@wapdc/common/mailer'
import sgMail from "@sendgrid/mail"
import {addRecipient, sendMailMerge, setMessageTemplate, setTestEmailAddress} from "../../Mailer.js";


// Mock implementation of sgMail from sendgrid
jest.mock('@sendgrid/mail')

describe('Mailer', () => {

    const ogEnv = JSON.parse(JSON.stringify(process.env))
    let lastMessage

    beforeEach(() => {
        sgMail.send = jest.fn().mockImplementation((message) => {
            lastMessage = message;
        })
    })

    afterEach(() => {
        process.env = JSON.parse(JSON.stringify(ogEnv))
    })

    /**
     * Handles single contact as a string and filters out non-PDC addresses when not running in the live environment.
     */
    test('Send single contact email', async () => {
        let contact = 'Timothy@pdc.wa.gov'
        const templateId = 'a-bbbbbbbbbbbbb'
        const templateData = {
            committee: 'friends of john jones',
            candidate: 'john jones'
        }

        const expectedCall = {
            to: 'Timothy@pdc.wa.gov',
            from: 'pdc@pdc.wa.gov',
            templateId: 'a-bbbbbbbbbbbbb',
            dynamicTemplateData: {
                committee: 'friends of john jones',
                candidate: 'john jones'
            }
        }

        // Calls when env != live with pdc address
        await sendMessage(contact, templateId, templateData)
        expect(sgMail.send).toHaveBeenCalledWith(expectedCall)

        // When env != live, does not send with non-pdc address
        contact = 'Timothy@notpdc.wa.gov'
        await sendMessage(contact, templateId, templateData)
        jest.clearAllMocks()
        expect(sgMail.send).not.toHaveBeenCalled()

        // When env = live, sends with non-pdc address
        process.env.STAGE = 'live'
        expectedCall.to = contact
        await sendMessage(contact, templateId, templateData)
        expect(sgMail.send).toHaveBeenCalledWith(expectedCall)
    })

    /**
     * Handles multiple contacts and filters out non-PDC addresses when not running in the live environment.
     */
    test('Send to multiple contacts', async () => {
        const templateId = 'a-bbbbbbbbbbbbb'
        const templateData = {
            committee: 'friends of john jones',
            candidate: 'john jones'
        }

        const expectedCall = {
            from: 'pdc@pdc.wa.gov',
            templateId: 'a-bbbbbbbbbbbbb',
            dynamicTemplateData: {
                committee: 'friends of john jones',
                candidate: 'john jones'
            }
        }

        // Sending where env != live filters out non PDC addresses
        let contact = ['Timothy@notpdc.wa.gov', 'sally@pdc.wa.gov', 'steve@PDc.wa.gov']
        expectedCall.to = ['sally@pdc.wa.gov', 'steve@PDc.wa.gov']
        await sendMessage(contact, templateId, templateData)
        expect(sgMail.send).toHaveBeenCalledWith(expectedCall)

        // When env != live, does not send when all addresses are non-pdc
        contact = ['Timothy@notpdc.wa.gov', 'sally@notpdc.wa.gov', 'steve@notPDc.wa.gov']
        await sendMessage(contact, templateId, templateData)
        jest.clearAllMocks()
        expect(sgMail.send).not.toHaveBeenCalled()

        // Sending where env = live includes non PDC addresses
        process.env.STAGE = 'live'
        contact = ['Timothy@notpdc.wa.gov', 'sally@pdc.wa.gov', 'steve@PDc.wa.gov']
        expectedCall.to = ['Timothy@notpdc.wa.gov', 'sally@pdc.wa.gov', 'steve@PDc.wa.gov']
        await sendMessage(contact, templateId, templateData)
        expect(sgMail.send).toHaveBeenCalledWith(expectedCall)
    })

    test('Mail merge using custom data as personalizatons', async() => {
        const expectedCall = {
            from: 'pdc@pdc.wa.gov',
            templateId: 'a-bbbbbbbbbbbbb',
            personalizations: [
                {
                    to: 'testa@noreply.com',
                    dynamicTemplateData: {
                        committee: 'Committee A'
                    }
                },
                {
                    to: 'testb@noreply.com',
                    dynamicTemplateData: {
                        committee: "Committee B"
                    }
                }
            ],
            mail_settings: {
                bypass_spam_management : {
                    enable: true
                },
                bypass_unsubscribe_management: {
                    enable: true
                }
            }
        }

        // Send message
        await setMessageTemplate('a-bbbbbbbbbbbbb', true)
        addRecipient('testa@noreply.com', {committee: "Committee A"})
        addRecipient('testb@noreply.com', {committee: "Committee B"})
        await sendMailMerge();
        expect(sgMail.send).toHaveBeenCalledWith(expectedCall)
    })

    test('Force demo sends to logged in/test user ', async () => {
      // Check message send function
      process.env.STAGE ='dev'
      setTestEmailAddress('user@pdc.wa.gov')
      await sendMessage('testa.noreply.com', 'a-12345', {committee: 'Comittee A'})
      expect(lastMessage.to).toBe('user@pdc.wa.gov')

      await setMessageTemplate('a-12345')
      addRecipient('test@noreply.com')
      await sendMailMerge()
      expect(lastMessage.personalizations[0].to).toBe('user@pdc.wa.gov')

    })
})