import { afterAll, beforeAll, describe, expect, test } from '@jest/globals';
import * as drafts from '../../Drafts.js';
import * as db from '../../WapdcDb.js';
import testData from '../data/Drafts.json';

let draftId;
const updatedPayload = {
  ...testData,
  user_data: {
    key1: 'updatedValue1',
    key2: 'updatedValue2'
  },
  save_count: testData.save_count + 1
};

beforeAll(async () => {
  await db.connect();
  await db.beginTransaction();
});

afterAll(async () => {
  await db.rollback();
  await db.close();
});

describe('Drafts CRUD functionality', () => {
  test('Can create a new draft', async () => {
    draftId = await drafts.createDraft(testData);
    expect(Number.isInteger(draftId)).toBe(true);
    expect(draftId).toBeGreaterThan(0);
  });

  test('Can retrieve a draft', async () => {
    const retrievedDraft = await drafts.getDraft(draftId);
    expect(retrievedDraft).toBeDefined();
  });

  test('Can update a draft with correct save count', async () => {
    const currentDraft = await drafts.getDraft(draftId);
    const updatedDraft = {
      ...updatedPayload,
      draft_id: draftId,
      save_count: currentDraft.save_count
    };

    const newSaveCount = await drafts.saveDraft(updatedDraft);
    expect(newSaveCount).toBe(updatedDraft.save_count + 1);
  });


  test('Throws error on updating with incorrect save count', async () => {
    await expect(drafts.saveDraft({
      ...testData,
      draft_id: draftId,
      save_count: 5
    })).rejects.toThrow('Save count mismatch');
  });

  test('Can delete a draft', async () => {
    await drafts.deleteDraft(draftId);
    const retrievedDraft = await drafts.getDraft(draftId);
    expect(retrievedDraft).toBeNull();
  });
});

describe('Drafts Report CRUD functionality', () => {
  test('Can create a new draft', async () => {
    draftId = await drafts.createDraft(testData);
    expect(Number.isInteger(draftId)).toBe(true);
    expect(draftId).toBeGreaterThan(0);
  });

  test('Can retrieve a draft', async () => {
    const retrievedDraft = await drafts.getReportDraft(testData.target_type, testData.target_id, testData.report_type, testData.report_key);
    expect(retrievedDraft).toBeDefined();
  });

  test('Can update a draft with correct save count', async () => {
    const currentDraft = await drafts.getReportDraft(testData.target_type, testData.target_id, testData.report_type, testData.report_key);
    const updatedDraft = {
      ...updatedPayload,
      save_count: currentDraft.save_count
    };

    const newSaveCount = await drafts.saveDraft(updatedDraft);
    expect(newSaveCount).toBe(updatedDraft.save_count + 1);
  });


  test('Throws error on updating with incorrect save count', async () => {
    await expect(drafts.saveDraft({
      ...testData,
      save_count: 5
    })).rejects.toThrow('Save count mismatch');
  });

  test('Can delete a draft', async () => {
    await drafts.deleteReportDraft(testData.target_type, testData.target_id, testData.report_type, testData.report_key);
    const retrievedDraft = await drafts.getReportDraft(testData.target_type, testData.target_id, testData.report_type, testData.report_key);
    expect(retrievedDraft).toBeNull();
  });
});


