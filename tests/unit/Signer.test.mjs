import {describe, expect, test} from '@jest/globals'
import * as signer from '@wapdc/common/signer'

describe('Signer', () => {
  const somePayload = {
    "committee_id": 100,
    "fund_id": 110,
    "etc": 'miscellaneous'
  }

  test('Can sign and verify data', async () => {
    const token = await signer.sign(somePayload, 'TokenSigner')

    {
      const payload = await signer.decode(token);

      // Check to ensure we can access without the correct key.
      expect(payload.committee_id).toBe(somePayload.committee_id)
    }

    {
      const payload = await signer.verify(token, 'TokenSigner')

      // Check to ensure we can access with the correct key.
      expect(payload).toMatchObject(somePayload)
    }

  })
})

