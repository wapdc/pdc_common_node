import {test, describe, expect} from "@jest/globals"
import {mockClient} from "aws-sdk-client-mock"
import {getSecrets} from "../../SecretsManager.js"
import {GetSecretValueCommand, SecretsManagerClient} from "@aws-sdk/client-secrets-manager"
import 'aws-sdk-client-mock-jest'

describe('Secrets Manager', () => {
    const expectedSecrets = { 'foo': 'foo'}

    const secretsManagerClient = mockClient(SecretsManagerClient)
    secretsManagerClient.on(GetSecretValueCommand).resolves({SecretString : JSON.stringify(expectedSecrets)})

    test('Retrieve secrets', async() => {

        const secrets = await getSecrets('testSecrets')

        //show that secretsManagerClient got called with 'testSecrets'
        expect(secretsManagerClient).toHaveReceivedCommandWith(GetSecretValueCommand, {SecretId : 'testSecrets'})
        //Exists
        expect(secrets).toBeTruthy()
        //Specific variable expectation
        expect(secrets.foo).toBe(expectedSecrets.foo)
    })
})