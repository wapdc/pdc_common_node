import API from "lambda-api"
import {connect, close} from "./WapdcDb.js"
import {readFile} from "fs/promises"
export const claims = {
    uid: null,
    realm: null,
    admin: false
}
let promiseToClose
/**
 * Function to set CORS headers.
 *
 * @param {object} res - The HTTP response object.
 */
export function setCORSHeaders(res) {
    res.header('Content-Type', 'application/json')
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Content-Type,Accept,Access-Control-Allow-Origin,Authorization')
    res.header('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT')
}

/**
 * Fetches user claims from either local file or from event authorizer.
 *
 * @param {object} req - The REQUEST object from lambda-api.
 * @returns {Promise<object>} - The promise to return a claims object.
 */
async function getClaims(req) {
    Object.keys(claims).forEach(key => delete claims[key]);
    if (process.env.STAGE === 'dev') {
        const claimsJson = await readFile('local/dev-jwt-claims.json')
        Object.assign(claims, JSON.parse(claimsJson))
    } else {
        Object.assign(claims, req.requestContext?.authorizer)
    }
}

export function getRestApi(config = {}) {
    const api = API(Object.assign({ version: 'v1.0' }, config))
    /**
     * Middleware to set CORS headers for OPTIONS requests.
     */
    api.options('/*', (req, res) => {
        setCORSHeaders(res)
        res.status(200).json({})
    })

    /**
     * Middleware to set CORS headers for all other requests.
     */
    api.use((req, res, next) => {
        setCORSHeaders(res)
        next()
    })


    /**
     * Middleware to check for valid user claims. If invalid, throw an error.
     */
    api.use(async (req, res, next) => {
        await getClaims(req)
        if (!claims.uid || !claims.realm)
        {
            console.error("Access denied claims", claims)
            res.error(403, "Access denied (claims)")
        } else {
            req.claims = claims
            await connect()
            next()
        }
    })

    /**
     * Middleware for error handling. Any thrown error will be caught here.
     */
    api.use((error, req, res, next) => {
        console.error(error)
        if (!res.headersSent) {
            setCORSHeaders(res)
            const statusCode = error.statusCode || 500
            const message =  'Unknown error, try again in a few minutes.'
            res.status(statusCode)
            res.json({ success: false, message })
        }
        // Technically we should never get here, but if we've already returned a response before the headers we might as well continue.
        else {
            next()
        }
    })

    // Finally does not support async functions, so we create a promise to be used as a semaphore to close the database
    // connection. This lets us wait for the database closing as part of hte request.
    api.finally( () => {
        promiseToClose = new Promise(resolve => {
            close().catch(() => resolve(false)).then(() => resolve(true))
        })
    })

    return api
}

/**
 * Wait for completion of finally steps and return whether the final api steps
 * were successful.  This should be called in all lambda handlers before returning
 * the response.
 *
 * @return {Promise<boolean>} true implies no errors. False implies errors.
 */
export async function waitForCompletion() {
   return await promiseToClose
}