# AWS Project setup

# Prerequisites

Prior to beginning, make sure you have teh AWS command line tools and hte SAM CLI installed locally on your workstation

# Create an initial project

Use npm to create a new npm project for use in the application. 

## Install the pdc_common_node library

First ensure that the project is pointed at the public disclosure commissions npm registry by adding the following line
to the .npmrc file that resides in the same directory as package.json:

```text
@wapdc:registry=https://gitlab.com/api/v4/groups/288013/-/packages/npm/
```
Install the library using npm.

```shell
npm i @wapdc/common
```

# Build out the minimal application 

- Create [admin_requests.mjs](../samples/admin_requests.mjs) and [filer_requests.mjs](../samples/filer_requests.mjs) files as appropriate for the project.
- Create serverless deployment template modelled after [the example](../samples/template.yaml)
- Create a samconfig.toml file modelled from [the example](../samples/samconfig.toml)

# Create the npm start-api command in the project

In project create a command for running the start-api function on a consistent port.  Note change the port number each 
time so that it doesn't conflict with any existing projects. 

```json
{"start-api": "sam local start-api --env-vars local/env.json  --port 3030"}
```

For the start-api command to function you need to create the following files according to the exmaples; 

- [local/env.json](../samples/env.json)
- [local/dev-jwt-claims.json](../samples/dev-jwt-claims.json)

# Deploy and test.

Using the sam cli build and deploy your code. 

```shell
sam build
sam deploy 
```
