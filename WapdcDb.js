import pg from 'pg'
import {getSecrets} from "./SecretsManager.js"
pg.types.setTypeParser(pg.types.builtins.DATE, str => str)


let db
let stage

export async function beginTransaction() {
  await db.query("BEGIN")
}

export async function commit() {
  await db.query("COMMIT")
}

export async function rollback() {
  await db.query("ROLLBACK")
}

export async function connect(){
    try{
      if(process.env.STAGE !== 'dev'){
        //pass in credentials for pg pool
        const dbSecretName = 'WapdcDb-' + process.env.STAGE
        const secretCredentials = await getSecrets(dbSecretName)
        db = new pg.Client({
          host: process.env.PGHOST ? process.env.PGHOST : secretCredentials.host,
          user: secretCredentials.username,
          password: secretCredentials.password,
          port: process.env.PGPORT ? process.env.PGPORT : secretCredentials.port,
          database: secretCredentials.dbname,
          ssl: {rejectUnauthorized: false},
        })

      }else{
        db =  new pg.Client({})
      }
      await db.connect()
      await getStage()
    }catch(err){
      console.error('An error occurred when opening the connection: ', err)
      throw Error(err)
    }
}

export async function close(){
  if(db){
    await db.end()
  }
}

export async function getStage() {
  const result = await db.query("SELECT value from wapdc_settings where property='stage'")
  stage = result.rows[0].value
  return stage
}

export async function getWapdcSetting(property) {
  const result = await db.query("SELECT value from wapdc_settings where stage=$1 and property=$2", [stage, property])
  let value = null
  if (result.rows && result.rows.length) {
    value = result.rows[0].value
  }
  return value
}

export async function setWapdcSetting(property, value) {
  const records = await db.query("SELECT value from wapdc_settings where stage = $1 and property=$2", [stage, property ])
  if (records && records.rows && records.rows.length) {
    await db.query("UPDATE wapdc_settings set value = $1::text WHERE stage=$2 and property = $3", [value, stage, property])
  }
  else {
    await db.query("INSERT into wapdc_settings(property, stage, value) values ($1, $2, $3)", [property, stage, value])
  }
  return {
    stage: stage,
    property: property,
    value: value
  }
}

export async function query(query, parameters){
  let {rows} = await db.query(query, parameters)
  return rows
}

export async function execute(query, parameters){
  await db.query(query, parameters)
}

export async function fetch(sql, parameters = []) {
  const result = await db.query(sql, parameters)
  if (result && result.rows.length > 0) {
    return result.rows[0]
  }
  else {
    return null
  }
}