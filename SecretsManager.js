import {GetSecretValueCommand, SecretsManagerClient} from "@aws-sdk/client-secrets-manager"

const secrets = {}

/**
 * Retrieve the secrets from the secrets manager in order or from process.env
 * @param secretId
 * @returns {Promise<*>}
 */
export async function getSecrets (secretId) {
    if (!secrets[secretId]) {
        const client = new SecretsManagerClient()
        const secret = await client.send(new GetSecretValueCommand({'SecretId': secretId}))
        secrets[secretId] = JSON.parse(secret.SecretString)
    }
    return secrets[secretId]
}